import React, { Component } from 'react';
import { connect } from 'react-redux';
import { formatter } from './ForMatter';

class ItemPhone extends Component {
    
    render() {
        let {tenSP , giaBan , hinhAnh } = this.props.phone ; 
        return (
            <div className='col-4'>
                <div className="card border-primary h-100 text-center">
                    <img className="card-img-top" src={hinhAnh} />
                    <div className="card-body">
                        <h4 className="card-title">{tenSP}</h4>
                        <p className="card-text">{formatter.format(giaBan)}</p>
                        <div className='d-flex justify-content-between'>
                            <button onClick={() => {
                                this.props.xemChiTiet(this.props.phone)
                            }} className="btn btn-outline-info">Xem chi tiết</button>
                            <button onClick={() => {
                                this.props.themGioHang(this.props.phone) 
                            }} className="btn btn-outline-primary">Thêm vào giỏ</button>
                        </div>
                    </div>
                </div>              
            </div>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        themGioHang : (sanPham) => {
            const spGioHang = {
                maSP : sanPham.maSP , 
                tenSP : sanPham.tenSP , 
                giaBan : sanPham.giaBan , 
                hinhAnh : sanPham.hinhAnh ,
                
            }
            const action = {
                type : 'THEM_GIO_HANG' , 
                spGioHang : spGioHang
            }
            dispatch(action) ; 
        } , 
        xemChiTiet : (sanPham) => {
            const action = {
                type : 'XEM_CHI_TIET' , 
                spdetailed : sanPham
            }
            dispatch(action) ; 
        }
    }
}

export default connect(null , mapDispatchToProps)(ItemPhone);