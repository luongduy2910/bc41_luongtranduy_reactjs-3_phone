import React, { Component } from 'react';
import ItemPhone from './ItemPhone';
import { data_phone } from './data_phone';


class ListPhone extends Component {
    render() {
        return (
            <div className='container p-5'>
                <h2 className='text-center'>List Phone</h2>
                <div className="row mt-5">
                    {data_phone.map((phone , index) => {
                        return (
                            <ItemPhone key={index} phone = {phone}/>
                        )
                    })}
                </div>
            </div>
        );
    }
}

export default ListPhone;