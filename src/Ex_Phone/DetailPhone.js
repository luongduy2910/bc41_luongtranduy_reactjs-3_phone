import React, { Component } from 'react';
import { detailReducer } from '../redux/reducer/DetailReducer';
import { connect } from 'react-redux';
import { formatter } from './ForMatter';
class DetailPhone extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let {maSP , tenSP , manHinh , heDieuHanh , cameraTruoc , cameraSau  ,giaBan , hinhAnh} = this.props.detail;
        return (
            <div className='container p-5'>
                <h2 className='text-center'>Chi tiết sản phẩm</h2>
                <div className="row mt-5">
                    <div className="col-5">
                        <h2 className='ml-5'>{tenSP}</h2>
                        <img className='w-100' src={hinhAnh} />
                    </div>
                    <div className="col-7">
                        <p className='my-4'>mã sản phẩm : {maSP}</p>
                        <hr />
                        <p className='my-4'>màn hình : {manHinh}</p>
                        <hr />
                        <p className='my-4'>hệ điều hành : {heDieuHanh}</p>
                        <hr />
                        <p className='my-4'>camera trước : {cameraTruoc}</p>
                        <hr />
                        <p className='my-4'>camera sau : {cameraSau}</p>
                        <hr />
                        <p className='my-4'>giá bán : {formatter.format(giaBan)}</p>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {detail : state.detailReducer.detail} 
}

export default connect(mapStateToProps , null)(DetailPhone);