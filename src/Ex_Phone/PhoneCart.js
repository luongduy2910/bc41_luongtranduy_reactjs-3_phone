import React, { Component } from 'react';
import { connect } from 'react-redux';
import { gioHangReducer } from '../redux/reducer/GioHangReducer';
import { formatter } from './ForMatter';

class PhoneCart extends Component {
    constructor(props) {
        super(props);
    }
    renderPhoneCartWithMap = () => {
        return this.props.gioHang.map((phone , index)=> {
            let {tenSP , maSP , giaBan , hinhAnh , soLuong} = phone;
            return (
                <tr key={index}>
                    <td>{maSP}</td>
                    <td>{tenSP}</td>
                    <td>{formatter.format(giaBan)}</td>
                    <td><img width={50} src={hinhAnh} /></td>
                    <td><button onClick={() => {
                        soLuong > 1 && this.props.thayDoiSoLuong(maSP , -1)
                    }} className="btn btn-outline-warning mr-2">-</button>{soLuong}<button onClick={() => {
                        this.props.thayDoiSoLuong(maSP , 1) 
                    }} className="btn btn-outline-warning ml-2">+</button></td>
                    <td>{formatter.format(giaBan * soLuong)}</td>
                    <td><button onClick={() => {
                        this.props.xoaGioHang(maSP) 
                    }} className="btn btn-danger">Delete</button></td>
                </tr>
            )
        })
    }
    render() {
        if (this.props.gioHang.length >= 1) {
            return (
                <div className='container p-5'>
                    <h2 className='text-center'>Phone Cart</h2>
                    <table className="table table-striped table-inverse mt-5">
                        <thead className="thead-inverse">
                            <tr>
                                <th>Mã sp</th>
                                <th>Tên sp</th>
                                <th>Giá Bán</th>
                                <th>Hình Ảnh</th>
                                <th>Số Lượng</th>
                                <th>Tổng tiền</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                {this.renderPhoneCartWithMap()}
                            </tbody>
                    </table>         
                </div>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {gioHang : state.gioHangReducer.gioHang} 
}

const mapDispatchToProps = (dispatch) => {
    return {
        xoaGioHang : (maSP) => {
            const action = {
                type : 'XOA_GIO_HANG' , 
                maSP : maSP
            }
            dispatch(action)
        },
        thayDoiSoLuong : (maSP , soLuong) => {
            const action = {
                type : 'THAY_DOI_SO_LUONG' , 
                maSP : maSP , 
                soLuong : soLuong
            }
            dispatch(action)
    }
}
}
export default connect(mapStateToProps , mapDispatchToProps , null)(PhoneCart);