const stateGioHang = {
    gioHang : [] ,  
}

export const gioHangReducer = (state = stateGioHang , action) => {
    switch (action.type) {
        case 'THEM_GIO_HANG' : {
            let clonegioHang = [...state.gioHang] ; 
            const index = clonegioHang.findIndex(spGH => spGH.maSP === action.spGioHang.maSP) ; 
            if (index == -1) {
                let clonespGioHang = {...action.spGioHang , soLuong : 1} ; 
                clonegioHang.push(clonespGioHang) ; 
            }else {
                clonegioHang[index].soLuong += 1 ; 
            }
            state.gioHang = clonegioHang ;
            return {...state} ;
        }
        case 'XOA_GIO_HANG' : {
            let clonegioHang = [...state.gioHang] ; 
            const filteredItem = clonegioHang.filter(item => item.maSP !== action.maSP) ; 
            state.gioHang = filteredItem ; 
            return {...state} ;
        }
        case 'THAY_DOI_SO_LUONG' : {
            let clonegioHang = [...state.gioHang] ;
            const viTri = clonegioHang.findIndex(item => item.maSP === action.maSP) ; 
            clonegioHang[viTri].soLuong += action.soLuong ; 
            state.gioHang = clonegioHang ; 
            return {...state} ;
        }
    }
    return {...state} 
}

